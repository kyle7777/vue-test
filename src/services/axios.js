// axios.js
import axios from 'axios'
// import { ElLoading, ElMessage } from 'element-plus'
// import { getTokenAUTH } from '@/utils/auth.js'

const LoadingInstance = {
  _target: null,
  _count: 0,
}

// function myAxios(axiosConfig, customOptions, loadingOptions) {
function myAxios(axiosConfig, customOptions) {
  const service = axios.create({
    baseURL: 'http://192.168.31.197:3000', // 设置统一的请求前缀
    timeout: 10000, // 设置统一的超时时长
  })

  // 自定义配置
  let custom_options = Object.assign(
    {
      repeat_request_cancel: true, // 是否开启取消重复请求, 默认为 true
      loading: false, // 是否开启loading层效果, 默认为false
      reduct_data_format: true, // 是否开启简洁的数据结构响应, 默认为true
      error_message_show: true, // 是否开启接口错误信息展示,默认为true
      code_message_show: false, // 是否开启code不为0时的信息提示, 默认为false
    },
    customOptions
  )

  // 请求拦截
  service.interceptors.request.use(
    (config) => {
      // 创建loading实例
      if (custom_options.loading) {
        LoadingInstance._count++
        if (LoadingInstance._count === 1) {
          // LoadingInstance._target = ElLoading.service(loadingOptions)
        }
      }
      // 自动携带token
      if (localStorage.getItem('token') && typeof window !== 'undefined') {
        config.headers.Authorization = localStorage.getItem('token')
      }

      return config
    },
    (error) => {
      return Promise.reject(error)
    }
  )

  return service(axiosConfig)
}

export default myAxios