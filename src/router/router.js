import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
	routes: [
		{
			path: '/',
			redirect: '/login'
		},
		{ 
			path: '/home', 
			name: 'home',
			component: () => import('../pages/Home')
		},
		{ 
			path: '/login', 
			name: 'login',
			component: () => import('../pages/login/login')
		},
	]
})

export default router