import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import antd from 'ant-design-vue'
// import myAxios from './services/axios'
import axios from 'axios'

import 'ant-design-vue/dist/antd.css'

Vue.config.productionTip = false
Vue.prototype.$message = antd.message

axios.interceptors.request.use(config => {
  let token = window.localStorage.getItem('token');
  if (token) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
      config.headers.Authorization = 'Bearer ' + token;
  }
  return config
}, error => {
  return Promise.reject(error)
})

Vue.prototype.$axios = axios
// Vue.prototype.$axios.defaults.headers.common['token'] = window.localStorage.getItem('token');

Vue.use(antd)

// const router = new VueRouter({
//   routes
// })
    
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
